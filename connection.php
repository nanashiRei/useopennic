<?php
$connection = 'btn';
include('template.php');
echo $HEADER_TEMPLATE;

if(isset($_GET["completed"])) {
//There will be a counter here in the future.
}

?> 

<div class="hero">
      <div class="container">
        <h1 style='line-height:65px;'>Test Your Connection</h1>
      </div>
    </div>
<div class="container content">
      <div class="row">
        <div class="span9">
          <h2>Let's See If You Are Connected!</h2>
          <p>
            On the right of the page, you will see a bunch of images (two for each OpenNIC Top Level Domain), your computer will scan through them and see if you are properly connecting to the domains, if you are they will light up and it means that you have successfully put OpenNIC on your computer. Now you should check out <a href='http://opennicproject.org'>The OpenNIC Project</a>'s website and sign up for the mailing list and take ownership of your DNS.
          </p>
          <h4>Restart Your Computer</h4>
		  <p>
            A common problem is that your DNS is cached on your computer or that your settings haven't been changed throughout all your programs. So if you restart your computer it may fix the problem.
          </p>
		  <h4>Check Your Settings</h4>
		  <p>
			Try going through our setup tool again and making sure that all your settings are proper to your computer.
		  </p>
		  <p>
            <a class="btn" href="setup.php">
              Setup Your Computer!
            </a>
          </p>
		  <h4>Ensure Your ISP Does Not Intercept DNS</h4>
          <p>
            Some ISPs actually capture DNS on their end, and then run your queries through their own DNS system. If this is the case you should contact them directly and ask why they are forcing their DNS choice upon you.</p>
		<p>
<?php
    $ip            = $_SERVER['REMOTE_ADDR'];
    $fullhost    = gethostbyaddr($ip);
    $gtlds        = array("edu", "gov", "mil", "aero", "asia", "biz", "cat", "com", "coop", "int", "info", "int", "jobs", "mobi", "museum", "name", "net", "org", "pro", "tel", "travel", "xxx");
    $cctlds        = array("ac", "ad", "ae", "af", "ag", "ai", "al", "am", "an", "ao", "aq", "ar", "as", "at", "au", "aw", "ax", "az", "ba", "bb", "bd", "be", "bf", "bg", "bh", "bi", "bj", "bm", "bn", "bo", "br", "bs", "bt", "bv", "bw", "by", "bz", "ca", "cc", "cd", "cf", "cg", "ch", "ci", "ck", "cl", "cm", "cn", "co", "cr", "cs", "cu", "cv", "cx", "cy", "cz", "dd", "de", "dj", "dk", "dm", "do", "dz", "ec", "ee", "eg", "eh", "er", "es", "et", "eu", "fi", "fj", "fk", "fm", "fo", "fr", "ga", "gb", "gd", "ge", "gf", "gg", "gh", "gi", "gl", "gm", "gn", "gp", "gq", "gr", "gs", "gt", "gu", "gw", "gy", "hk", "hm", "hn", "hr", "ht", "hu", "id", "ie", "il", "im", "in", "io", "iq", "ir", "is", "it", "je", "jm", "jo", "jp", "ke", "kg", "kh", "ki", "km", "kn", "kp", "kr", "kw", "ky", "kz", "la", "lb", "lc", "li", "lk", "lr", "ls", "lt", "lu", "lv", "ly", "ma", "mc", "md", "me", "mg", "mh", "mk", "ml", "mm", "mn", "mo", "mp", "mq", "mr", "ms", "mt", "mu", "mv", "mw", "mx", "my", "mz", "na", "nc", "ne", "nf", "ng", "ni", "nl", "no", "np", "nr", "nu", "nz", "om", "pa", "pe", "pf", "pg", "ph", "pk", "pl", "pm", "pn", "pr", "ps", "pt", "pw", "py", "qa", "re", "ro", "rs", "ru", "rw", "sa", "sb", "sc", "sd", "se", "sg", "sh", "si", "sj", "sk", "sl", "sm", "sn", "so", "sr", "ss", "st", "su", "sv", "sy", "sz", "tc", "td", "tf", "tg", "th", "tj", "tk", "tl", "tm", "tn", "to", "tp", "tr", "tt", "tv", "tw", "tz", "ua", "ug", "uk", "us", "uy", "uz", "va", "vc", "ve", "vg", "vi", "vn", "vu", "wf", "ws", "ye", "yt", "yu", "za", "zm", "zw", "xn--lgbbat1ad8j", "xn--54b7fta0cc", "xn--fiqs8s", "xn--fiqz9s", "xn--wgbh1c", "xn--node", "xn--j6w193g", "xn--h2brj9c", "xn--mgbbh1a71e", "xn--fpcrj9c3d", "xn--gecrj9c", "xn--s9brj9c", "xn--xkc2dl3a5ee0h", "xn--45brj9c", "xn--mgba3a4f16a", "xn--mgbayh7gpa", "xn--80ao21a", "xn--mgbx4cd0ab", "xn--mgbc0a9azcg", "xn--mgb9awbf", "xn--mgbai9azgqp6j", "xn--ygbi2ammx", "xn--wgbl6a", "xn--p1ai", "xn--mgberp4a5d4ar", "xn--90a3ac", "xn--yfro4i67o", "xn--clchc0ea0b2g2a9gcd", "xn--3e0b707e", "xn--fzc2c9e2c", "xn--xkc2al3hye2a", "xn--mgbtf8fl", "xn--kprw13d", "xn--kpry57d", "xn--o3cw4h", "xn--pgbs0dh", "xn--j1amh", "xn--mgbaam7a8h", "xn--mgb2ddes");
    $host        = explode(".", $fullhost);
    $hosttemp    = array();
    $tldcount    = 0;
    $otc        = 0;
    $break        = false;
    
    $host = array_reverse($host);
    foreach ($host as $h)
    {
        if (!$break) {
            $otc = $tldcount;
            foreach ($gtlds as $g)
            {
                if ($h == $g) { $tldcount++; }
            }
            
            foreach ($cctlds as $c)
            {
                if ($h == $c) { $tldcount++; }
            }
            if (!($tldcount > $otc))
                $break = true;
        }
    }
    if ($tldcount == 0)
        echo "Our System Is Unable To Identify Your ISP, <br /> According To Us, Your Hostname Is: $host";

    else
    {
        for ($i=0; $i <= $tldcount; $i++)
        {
            $hosttemp[$i] = $host[$i];
        }
        $host = array_reverse($hosttemp);

        foreach ($host as $v)
        {
            $hst .= $v .".";
        }
        echo "Our System Says Your ISP Is: $hst";
    }
?>
</p>
        </div>
           <div class="row">
<div style="text-align: center;">
	<p><strong>Click To Retest</strong> <input id=boton type=submit onclick=revisar(); value="Check"></p>
	<p>The Left Is IPv4 The Right Is IPv6</p>
</div>
    <div class="span7">
          <hr>
           <div class="row">
            <div class="span3">
       
<h3>BBS TLD</h3>
<img id=imagen_bbs name=img_tld src='http://maximi89.linuxerz.org/wp-content/uploads/2011/07/opennic_duda.png'></img>
<img id=imagen_bbs_ipv src='http://maximi89.linuxerz.org/wp-content/uploads/2011/07/opennic_duda.png'></img>


<h3>GOPHER TLD</h3>
<img id=imagen_gopher src='http://maximi89.linuxerz.org/wp-content/uploads/2011/07/opennic_duda.png'></img>
<img id=imagen_gopher_ipv src='http://maximi89.linuxerz.org/wp-content/uploads/2011/07/opennic_duda.png'></img>


<h3>FUR TLD</h3>
<img id=imagen_fur name=img_tld src='http://maximi89.linuxerz.org/wp-content/uploads/2011/07/opennic_duda.png'></img>
<img id=imagen_fur_ipv src='http://maximi89.linuxerz.org/wp-content/uploads/2011/07/opennic_duda.png'></img>


<h3>OSS TLD</h3>
<img id=imagen_oss src='http://maximi89.linuxerz.org/wp-content/uploads/2011/07/opennic_duda.png'></img>
<img id=imagen_oss_ipv src='http://maximi89.linuxerz.org/wp-content/uploads/2011/07/opennic_duda.png'></img>


<h3>PARODY TLD</h3>
<img id=imagen_parody src='http://maximi89.linuxerz.org/wp-content/uploads/2011/07/opennic_duda.png'></img>
<img id=imagen_parody_ipv src='http://maximi89.linuxerz.org/wp-content/uploads/2011/07/opennic_duda.png'></img>


<h3>FREE TLD</h3>
<img id=imagen_free name=img_tld src='http://maximi89.linuxerz.org/wp-content/uploads/2011/07/opennic_duda.png'></img>
<img id=imagen_free_ipv src='http://maximi89.linuxerz.org/wp-content/uploads/2011/07/opennic_duda.png'></img>

<h3>GEEK TLD</h3>
<img id=imagen_geek name=img_tld src='http://maximi89.linuxerz.org/wp-content/uploads/2011/07/opennic_duda.png'></img>
<img id=imagen_geek_ipv src='http://maximi89.linuxerz.org/wp-content/uploads/2011/07/opennic_duda.png'></img>


<h3>INDY TLD</h3>
<img id=imagen_indy src='http://maximi89.linuxerz.org/wp-content/uploads/2011/07/opennic_duda.png'></img>
<img id=imagen_indy_ipv src='http://maximi89.linuxerz.org/wp-content/uploads/2011/07/opennic_duda.png'></img>


            </div>
            <div class="span4">



<h3>NULL TLD</h3>
<img id=imagen_null src='http://maximi89.linuxerz.org/wp-content/uploads/2011/07/opennic_duda.png'></img>
<img id=imagen_null_ipv src='http://maximi89.linuxerz.org/wp-content/uploads/2011/07/opennic_duda.png'></img>

<h3>MICRO TLD</h3>
<img id=imagen_micro src='http://maximi89.linuxerz.org/wp-content/uploads/2011/07/opennic_duda.png'></img>
<img id=imagen_micro_ipv src='http://maximi89.linuxerz.org/wp-content/uploads/2011/07/opennic_duda.png'></img>


<h3>ING TLD</h3>
<img id=imagen_ing src='http://maximi89.linuxerz.org/wp-content/uploads/2011/07/opennic_duda.png'></img>
<img id=imagen_ing_ipv src='http://maximi89.linuxerz.org/wp-content/uploads/2011/07/opennic_duda.png'></img>


<h3>DYN TLD*</h3>
<img id=imagen_dyn src='http://maximi89.linuxerz.org/wp-content/uploads/2011/07/opennic_duda.png'></img>
<img id=imagen_dyn_ipv src='http://maximi89.linuxerz.org/wp-content/uploads/2011/07/opennic_duda.png'></img>


<h3>P2P TLD*</h3>
<img id=imagen_p2p src='http://maximi89.linuxerz.org/wp-content/uploads/2011/07/opennic_duda.png'></img>

<img id=imagen_p2p_ipv src='http://maximi89.linuxerz.org/wp-content/uploads/2011/07/opennic_duda.png'></img>


<h3>BIT TLD*</h3>
<img id=imagen_bit src='http://maximi89.linuxerz.org/wp-content/uploads/2011/07/opennic_duda.png'></img>
<img id=imagen_bit_ipv src='http://maximi89.linuxerz.org/wp-content/uploads/2011/07/opennic_duda.png'></img>


<h3>BZH TLD*</h3>
<img id=imagen_bzh src='http://maximi89.linuxerz.org/wp-content/uploads/2011/07/opennic_duda.png'></img>
<img id=imagen_bzh_ipv src='http://maximi89.linuxerz.org/wp-content/uploads/2011/07/opennic_duda.png'></img>

            </div>
          
  
          </div>
		  <hr>
          <p><i>*These TLDs are not currently supported by OpenNIC</i></p>
	<p>If you see any green check marks you are set, this feature isn't fully completed and as such not all the images are working yet.</p>
        </div>
      </div>
    </div>
	<?php
echo $FOOTER_TEMPLATE;
?>
